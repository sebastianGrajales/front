import { Component, OnInit } from '@angular/core';
import {ApiService} from './servicios/api.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  listaSECOP = [];

  ngOnInit():void{
  }
  constructor(private api:ApiService){
    this.generarListaSECOP();
  }
  
  private generarListaSECOP():void{
    this.api.obtenerDatosSECOP().subscribe(res=>{
      for(const key in res){
        this.listaSECOP.push({
          nivel: res[key].nivel_entidad,
          nombre: res[key].nombre_de_la_entidad,
          estado: res[key].estado_del_proceso
        })
      }
    }).unsubscribe;
  }
}
